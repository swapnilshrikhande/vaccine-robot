File : `src/test/java/in/cowin/robot/MainPageTest.java`

Update Pincode Array and Area Label Map to match your requirement

```
private static final String[] PIN_CODES = new String[]{
                "411040" // wanowarie
            ,   "411038" // kothrud
            ,   "411011" // Mangalwar Peth
            ,   "411048" // kondhawa
    };

    public static final Map<String,String> AREA = Map.of(   
            "411040"    , "Wanowarie"
        ,   "411038"    , "Kothrud"
        ,   "411011"    , "Mangalwar Peth"
        ,   "411048"    , "Kondhawa"
        ,   "425418"    , "Nandurbar" //for testing, as there are always some slots available here
    );

```

Run `mvn clean install`

