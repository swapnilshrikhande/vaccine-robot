package in.cowin.robot;

import in.cowin.robot.page.MainPage;
import com.github.webdriverextensions.WebSite;
import static com.github.webdriverextensions.Bot.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CowinSite extends WebSite {

    // Logger
    private static final Logger log = LoggerFactory.getLogger(CowinSite.class);

    // Url
    public static String url = "https://www.cowin.gov.in/home";

    // Pages
    public MainPage mainPage;
    // ...add your Site's WebPages here

    public void open(Object... arguments) {
        open(url);
    }

    @Override
    public void assertIsOpen(Object... arguments) throws Error {
        assertCurrentUrlStartsWith(url);
    }

}
