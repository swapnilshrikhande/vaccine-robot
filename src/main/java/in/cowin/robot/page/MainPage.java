package in.cowin.robot.page;

import in.cowin.robot.CowinSite;
import in.cowin.robot.component.ExampleWebComponent;
import com.github.webdriverextensions.WebPage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import static com.github.webdriverextensions.Bot.*;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class MainPage extends WebPage {

    // Logger
    private static final Logger log = LoggerFactory.getLogger(MainPage.class);

    // Url
    public String url = CowinSite.url;

    // Model
    @FindBy(css = ".pin-search input")
    public WebElement pinInput;

    @FindBy(css = ".pin-search-btn")
    public WebElement searchByPinButton;

    public List<WebElement> vaccineSlots;
    // ...add your Page's WebElements and WebComponents here

    @Override
    public void open(Object... arguments) {
        open(url);
        assertIsOpen();
    }

    @Override
    public void assertIsOpen(Object... arguments) throws Error {
        assertCurrentUrlStartsWith(url);
        // ...add your asserts that ensures that the page is loaded
    }

    public Boolean queryVaccineSlots(){
        this.vaccineSlots = driver().findElements(By.cssSelector(".center-box .vaccine-box a"));

        return this.vaccineSlots.isEmpty() == false;
    }

    public void search(String pinCode){
        pinInput.sendKeys(pinCode);
        if( searchByPinButton.isEnabled() && searchByPinButton.isDisplayed() ) {
            searchByPinButton.click();
        } else {
            System.out.println("Search By Pin Button Not Clickable");
        }

    }

    public void clearPinInput(){
        pinInput.clear();
    }

    public Integer getTotalSlots(){

        Integer totalSlots = vaccineSlots.stream().map((element)-> {

            String slotString = element.getText().trim();

            if( isNumeric(slotString) ){
                return Integer.parseInt(slotString);
            } else {
                return 0;
            }

        }).reduce(0,(sum, currentValue) -> {
            return sum+currentValue;
        });

        return totalSlots;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Integer.parseInt(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public void nextWeek() {
        WebElement nextWeekButton = driver().findElement(By.cssSelector("a.carousel-control-next"));

        if( nextWeekButton!=null && nextWeekButton.isDisplayed() ) {
            nextWeekButton.click();
        }
    }

    public void prevWeek() {
        WebElement nextWeekButton = driver().findElement(By.cssSelector("a.carousel-control-next"));

        if( nextWeekButton!=null && nextWeekButton.isDisplayed() ) {
            nextWeekButton.click();
        }
    }
}
