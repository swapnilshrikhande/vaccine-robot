package in.cowin.robot;

import in.cowin.audio.AudioPlayer;
import in.cowin.robot.CowinSiteTest;
import org.junit.Test;
import static com.github.webdriverextensions.Bot.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class MainPageTest extends CowinSiteTest {

    // Logger
    private static final Logger log = LoggerFactory.getLogger(MainPageTest.class);

    private static final String[] PIN_CODES = new String[]{
                "411040" // wanowarie
            ,   "411038" // kothrud
            ,   "411011" // Mangalwar Peth
            ,   "411048" // kondhawa
    };

    public static final Map<String,String> AREA = Map.of(   
            "411040" , "Wanowarie"
        ,   "411038"    , "Kothrud"
        ,   "411011"    , "Mangalwar Peth"
        ,   "411048"    , "Kondhawa"
        ,   "425418"    , "Nandurbar" //for testing, as there are always slots available
    );

    @Test
    public void openMainPageTest() throws Exception {
        open(site);
        assertIsOpen(mainPage);
        Integer totalSlots = 0;
        Boolean isFirstWeek = true;
        Boolean slotFound   = false;

        do {

            String currentWeek    = isFirstWeek?"Current":"Next";

            for(String pinCode : PIN_CODES) {
                totalSlots = 0;
                mainPage.clearPinInput();
                waitFor(1);
                mainPage.search(pinCode);
                waitFor(2);
                Boolean slotsRendered = mainPage.queryVaccineSlots();
                slotFound = false;

                if (slotsRendered) {
                    totalSlots = mainPage.getTotalSlots();

                    if( totalSlots > 0 ) {
                        System.out.println(" Tx : " + Calendar.getInstance().getTime().toString());
                        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        System.out.println("Total Slots Available = " + totalSlots + " @ " + AREA.get(pinCode)+" Week : "+currentWeek);
                        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        AudioPlayer.playSound();
                        slotFound = true;
                    }
                }

                if( !slotFound ) {
                    System.out.println("No Vaccination center is available for booking @ "+AREA.get(pinCode)+" Week : "+currentWeek);
                }

                waitFor(20);
            }

            //current logic toggles between current and next week slots
            // move to next week

            /*if( totalSlots == 0 ) {
                if( isFirstWeek ) {
                    mainPage.nextWeek();
                    isFirstWeek = false;
                }  else {
                    mainPage.prevWeek();
                    isFirstWeek = true;
                }
            }*/

        } while( true );

    }
}

